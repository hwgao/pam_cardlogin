#include <security/pam_modules.h>
#include <PCSC/wintypes.h>
long get_current_card_id (pam_handle_t *pamh, struct pam_conv *conv,
			  const int reader_number, 
			  const BYTE **card_id);
