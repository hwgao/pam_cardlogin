/*
 *  PAM CARDlogin Module 
 *  Copyright (C) 2010 Frank Engler <pam_CARD@web.de>,
 *
 *  This library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * This library is based on pam_p11 by Mario Strasser <mast@gmx.net>
 * and on the sample program to use PC/SC API by Ludovic Rousseau
 * <ludovic.rousseau@free.fr>.
 */

#include <syslog.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

/* We have to make this definitions before we include the pam header files! */
#define PAM_SM_AUTH
#define PAM_SM_ACCOUNT
#define PAM_SM_SESSION
#define PAM_SM_PASSWORD
#include <security/pam_modules.h>
#ifdef HAVE_SECURITY_PAM_EXT_H
#include <security/pam_ext.h>
#else
#define pam_syslog(handle, level, msg...) syslog(level, ## msg)
#endif

#ifndef PAM_EXTERN
#define PAM_EXTERN extern
#endif

#include "get_current_card_id.h"
#include "config.h"

static long get_stored_card_id (const char *user_name, const char **card_id);

PAM_EXTERN int pam_sm_authenticate(pam_handle_t * pamh, int flags, int argc,
				   const char **argv)
{
  int i, j, reader;
  long rv;
  const char *user, *user_card_id;
  const unsigned char *current_card_id;
  struct pam_conv *conv;

  /* get conversation function */
  pam_get_item(pamh, PAM_CONV, (const void **)&conv);

  /* check parameters */
  if (argc != 1) {
    pam_syslog(pamh, LOG_ERR,
	       "need card-reader-no. as argument");
    return PAM_ABORT;
  }
  reader = atoi(argv[0]);

  /* get user name */
  rv = pam_get_user(pamh, &user, NULL);
  if (rv != PAM_SUCCESS) {
    pam_syslog(pamh, LOG_ERR, "pam_get_user() failed %s",
	       pam_strerror(pamh, rv));
    return PAM_USER_UNKNOWN;
  }
  
/* get user's card id */
  rv = get_stored_card_id(user, &user_card_id);
  if (rv != 0)
    return rv;      /* error message already done; forward rv */

  /* get card id of inserted card */
  rv = get_current_card_id(pamh, conv, reader, &current_card_id);
  if (rv != 0)
    return rv; /* error message already done; forward rv */
  
  /* compare card ID with value in mapping-file */
  j = 0;
  for (i=0; i<=13; i++)
    j += (unsigned char)current_card_id[i] ^ (unsigned char)user_card_id[i];
  if (j==0)
    return PAM_SUCCESS;
  else
    return PAM_AUTH_ERR;
}

PAM_EXTERN int pam_sm_setcred(pam_handle_t * pamh, int flags, int argc,
			      const char **argv)
{
  /* Actually, we should return the same value as pam_sm_authenticate(). */
  return PAM_SUCCESS;
}

PAM_EXTERN int pam_sm_acct_mgmt(pam_handle_t * pamh, int flags, int argc,
				const char **argv)
{
  pam_syslog(pamh, LOG_WARNING,
	     "Function pam_sm_acct_mgmt() is not implemented in this module");
  return PAM_SERVICE_ERR;
}

PAM_EXTERN int pam_sm_open_session(pam_handle_t * pamh, int flags, int argc,
				   const char **argv)
{
  pam_syslog(pamh, LOG_WARNING,
	     "Function pam_sm_open_session() is not implemented in this module");
  return PAM_SERVICE_ERR;
}

PAM_EXTERN int pam_sm_close_session(pam_handle_t * pamh, int flags, int argc,
				    const char **argv)
{
  pam_syslog(pamh, LOG_WARNING,
	     "Function pam_sm_close_session() is not implemented in this module");
  return PAM_SERVICE_ERR;
}

PAM_EXTERN int pam_sm_chauthtok(pam_handle_t * pamh, int flags, int argc,
				const char **argv)
{
  pam_syslog(pamh, LOG_WARNING,
	     "Function pam_sm_chauthtok() is not implemented in this module");
  return PAM_SERVICE_ERR;
}

static long get_stored_card_id (const char *user_name, const char **card_id)
{
  FILE *fd;
  char buffer[LINE_MAX];
  static char *tmp_card_id = NULL;

  /* get mapping-file */
  fd = fopen(CARDLOGIN_USER_FILE, "r");
  if (fd == NULL) {
    pam_syslog(pamh, LOG_ERR, "fatal: cannot open mapping-file %s",
	       CARDLOGIN_USER_FILE);
    return PAM_AUTHINFO_UNAVAIL;
  }

  /* find user entry in mapping-file */
  while (fgets (buffer, LINE_MAX, fd) != NULL) {
    if (strncmp(buffer, user_name, strlen(user_name)) == 0) {
      tmp_card_id = strdup(&buffer[strlen(user_name)+1]);
      if (tmp_card_id[strlen(tmp_card_id)-1] == '\n')
	tmp_card_id[strlen(tmp_card_id)-1] = '\0';
      break;
    }
  }

  /* close mapping file */
  fclose(fd);

  /* check if card_id was found in mapping-file */
  if (tmp_card_id == NULL) {
    pam_syslog(pamh, LOG_ERR, "Cannot find user %s in %s",
	       user_name, CARDLOGIN_USER_FILE);
    return PAM_USER_UNKNOWN;
  }
  *card_id = tmp_card_id;
  return 0;
}

#ifdef PAM_STATIC
/* static module data */
struct pam_module _pam_group_modstruct = {
	"pam_cardlogin",
	pam_sm_authenticate,
	pam_sm_setcred,
	pam_sm_acct_mgmt,
	pam_sm_open_session,
	pam_sm_close_session,
	pam_sm_chauthtok
};
#endif
